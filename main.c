#include <SDL2/SDL.h>
#include <SDL2/SDL_rwops.h>
#include <SDL2/SDL_image.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void TGAwrite(SDL_RWops* file, SDL_Surface* surface)
{
    unsigned char targaMagic[12] = { 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    
    SDL_RWwrite(file, targaMagic, 1, sizeof(targaMagic));

    unsigned short width = (unsigned short)surface->w;
    unsigned short height = (unsigned short)surface->h;
    
    unsigned char pixelSize = 24;
    unsigned char flags = 0x0;
    
    SDL_RWwrite(file, &width, sizeof(unsigned short), 1);
    SDL_RWwrite(file, &height, sizeof(unsigned short), 1);
    SDL_RWwrite(file, &pixelSize, sizeof(unsigned char), 1);
    SDL_RWwrite(file, &flags, sizeof(unsigned char), 1);
    
    SDL_LockSurface(surface);
    int i;
    for (i=height-1; i>=0; i--)
        SDL_RWwrite(file, surface->pixels+(i*surface->pitch), 1, surface->pitch);
    SDL_UnlockSurface(surface);
}

void printHelp(const char* program)
{
    fprintf(stdout, "Usage:\n  %s <input file> [options]\n", program);
    fprintf(stdout, "Options:\n");
    fprintf(stdout, "  -d backgrounddir    write tga files to directory backgroundir\n");
    fprintf(stdout, "  -f backgroundfile   write information about background sizes to file backgroundfile\n");
    fprintf(stdout, "                      if not specified, backgrounddir/../BackgroundLayout.txt will be used\n");
    fprintf(stdout, "  -h                  show this message\n");
}

int main(int argc, char** argv)
{
    if (argc < 2)
    {
        fprintf(stderr, "Error: no arguments\n");
        fprintf(stderr, "Type '%s -h' to get help\n", argv[0]);
        return EXIT_FAILURE;
    }
    
    char* imageFile = NULL;
    char* outputDirectory = NULL;
    char* outputFile = NULL;
    
    int argi;
    for (argi = 1; argi < argc; ++argi)
    {
        char* opt = argv[argi];
        if (opt[0] == '-')
        {
            if (opt[1] == 'd')
            {
                argi++;
                if (argi >= argc)
                {
                    fprintf(stderr, "Directory name expected after -d\n");
                    return EXIT_FAILURE;
                }
                opt = argv[argi];
                if (outputDirectory)
                {
                    fprintf(stderr, "Ouput directory is already set to %s\n", outputDirectory);
                    return EXIT_FAILURE;
                }
                outputDirectory = opt;
            }
            else if (opt[1] == 'f')
            {
                argi++;
                if (argi >= argc)
                {
                    fprintf(stderr, "File name expected after -f\n");
                    return EXIT_FAILURE;
                }
                opt = argv[argi];
                if (outputFile)
                {
                    fprintf(stderr, "Ouput file is already set to %s\n", outputFile);
                    return EXIT_FAILURE;
                }
                outputFile = opt;
            }
            else if (opt[1] == 'h')
            {
                printHelp(argv[0]);
                return EXIT_SUCCESS;
            }
            else
            {
                fprintf(stderr, "Unknown option: %s\n", opt);
                return EXIT_FAILURE;
            }
        }
        else
        {
            if (imageFile)
            {
                fprintf(stderr, "Image file is already set to %s\n", imageFile);
                return EXIT_FAILURE;
            }
            imageFile = opt;
        }
    }
    
    if (!imageFile)
    {
        fprintf(stderr, "No input file specified. Exiting\n");
        return EXIT_FAILURE;
    }
    
    if (!outputDirectory)
    {
        fprintf(stderr, "No output directory specified. Exiting\n");
        return EXIT_FAILURE;
    }
    else
    {
        int len = strlen(outputDirectory);
        if (outputDirectory[len-1] == '/')
            outputDirectory[len-1] = '\0';
    }
    
    char outputFileBuf[512];
    if (!outputFile)
    {
        snprintf(outputFileBuf, sizeof(outputFileBuf), "%s/../BackgroundLayout.txt", outputDirectory);
        outputFile = outputFileBuf;
    }
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return EXIT_FAILURE;
    }
    
    IMG_Init(0);
    
    SDL_RWops* imageRWop = SDL_RWFromFile(imageFile, "rb");
    if (!imageRWop)
    {
        fprintf(stderr, "%s\n", SDL_GetError());
        return EXIT_FAILURE;
    }
    
    SDL_Surface* background = IMG_Load_RW(imageRWop, 1);
    
    if (!background)
    {
        fprintf(stderr, "%s: %s\n", imageFile, IMG_GetError());
        return EXIT_FAILURE;
    }
    
    
    const int maxSize = 256;
    
    int x, y, width, height, currentHeight, currentWidth;
    x = 0;
    y = 0;
    width = background->w;
    height = background->h;
    currentHeight = 0;
    currentWidth = 0;
    int i = 1;
    char j = 'a';
    
    const char* backgroundTxt = outputFile;
    FILE* txtFile = fopen(backgroundTxt, "wb");
    if (!txtFile)
    {
        fprintf(stderr, "Could not open %s\n", backgroundTxt);
        return EXIT_FAILURE;
    }
    
    fprintf(txtFile, "resolution\t%d\t%d\r\n\r\n", width, height);
    
    Uint32 rmask, gmask, bmask, amask;
    rmask = 0x00ff0000;
    gmask = 0x0000ff00;
    bmask = 0x000000ff;
    amask = 0x00000000;
    
    while (y < height)
    {
        if (y + maxSize > height)
            currentHeight = height-y;
        else
            currentHeight = maxSize;
        
        while(x < width)
        {
            if (x + maxSize > width)
                currentWidth = width-x;
            else
                currentWidth = maxSize;
            
            SDL_Surface* surface = SDL_CreateRGBSurface(0, currentWidth, currentHeight, 24, rmask, gmask, bmask, amask);
            
            if (!surface)
            {
                fprintf(stderr, "Failed to create surface: %s\n", SDL_GetError());
                return EXIT_FAILURE;
            }
            
            SDL_Rect rect;
            rect.x = x;
            rect.y = y;
            rect.w = currentWidth;
            rect.h = currentHeight;
            
            if (SDL_BlitSurface(background, &rect, surface, NULL) != 0)
            {
                fprintf(stderr, "Could not to blit surface: %s\n", SDL_GetError());
                SDL_FreeSurface(surface);
                return EXIT_FAILURE;
            }
            
            char fileName[512];
            snprintf(fileName, sizeof(fileName), "%s/%d_%d_%c_loading.tga", outputDirectory, width, i, j);
            SDL_RWops* outFile = SDL_RWFromFile(fileName, "wb");
            if (!outFile)
            {
                fprintf(stderr, "%s\n", SDL_GetError());
                return EXIT_FAILURE;
            }
            TGAwrite(outFile, surface);
            SDL_FreeRW(outFile);
            
            fprintf(txtFile, "resource/background/%d_%d_%c_loading.tga\tscaled\t%d\t%d\r\n", width, i, j, x, y);
            
            x += maxSize;
            j++;
            
            SDL_FreeSurface(surface);
        }
        
        fprintf(txtFile, "\r\n");
        
        y += maxSize;
        i++;
        x = 0;
        j = 'a';
    }
    
    fclose(txtFile);
    
    SDL_FreeSurface(background);
    
    IMG_Quit();
    SDL_Quit();
    return EXIT_SUCCESS;
}
