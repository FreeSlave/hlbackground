# Half-Life Background Generator

## Description

Small tool to generate necessary files for Half-Life background from one image file.

## Dependencies

Program uses SDL2_image library to load images, so the dependencies are SDL2 and SDL2_image.

## How to build

To keep it simple there is no Makefile. Just one small shell script called compile.sh

    chmod +x compile.sh
    ./compile.sh

You may want to rewrite shell script to specify include directories for SDL2 or set some other arguments to compiler.
